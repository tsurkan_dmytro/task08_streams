package epam.task.gener.textreader;


public class UniqueWordsView {
    UniqueWordsController controller;

    public UniqueWordsView() {
        controller = new UniqueWordsController();
    }

    public void  showUniqWords()  {
        System.out.println("There are " + controller.uniqWords() + " unique words in data.txt");
    }

    public void  showSortedUniqWords() {
        System.out.println("Sorted uniq words: " + controller.sortUniqWords());
    }

    public void showCollectData() {
        System.out.println("Collect dsts: " + controller.collectData());
    }
}
