package epam.task.gener.textreader;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingInt;


public class UniqueWordsModel {

        private Path path ;

        public UniqueWordsModel() {
            path = Paths.get("file.txt");
            this.writeFile(path);
        }

    private void writeFile(Path path) {
        try (BufferedWriter writer = Files.newBufferedWriter(path))
        {
            writer.write(TextData.text);
        } catch (IOException e){

        }
    }

    public long uniqWords()  {
        try {
            long uniqueWords = Files.lines(Paths.get(String.valueOf(path)), Charset.defaultCharset())
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .distinct()
                    .count();
            return  uniqueWords;
        }catch (IOException e){
            System.out.println("logggggg");
        }
        //System.out.println("There are " + uniqueWords + " unique words in data.txt");
        return 0;
    }

    public List<String> getListWords() {
            try {
                return Files.lines(Paths.get(String.valueOf(path)), Charset.defaultCharset())
                        .flatMap(line -> Arrays.stream(line.split(" ")))
                        .collect(Collectors.toList());
            }catch (IOException e){
                System.out.println("Logg");
            }
            return null;
    }

    public List<String> sortUniqWords()  {
        try {
            List<String> sortUniqWords =  Files.lines(Paths.get(String.valueOf(path)), Charset.defaultCharset())
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());

            return sortUniqWords;
        }catch (IOException e){
            System.out.println("LOGG");
        }
        return null;

    }

    public Map<String, Integer> collectData(List<String> listWords) {
        Map<String, Integer> collect = listWords
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), summingInt(e -> 1)));
        return collect;

    }
}
