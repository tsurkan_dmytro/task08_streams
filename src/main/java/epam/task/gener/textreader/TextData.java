package epam.task.gener.textreader;

public class TextData {
    static final String text = "1. “The Bogey Beast” by Flora Annie Steel\n" +
            "Reading Level: Very Easy\n" +
            "\n" +
            "A woman finds a pot of treasure on the road while she is returning from work. Delighted with her luck, she decides to keep it. As she is taking it home, it keeps changing. However, her enthusiasm refuses to fade away.\n" +
            "\n" +
            "What Is Great About It: The old lady in this story is one of the most cheerful characters anyone can encounter in English fiction. Her positive disposition (personality) tries to make every negative transformation seem like a gift, and she helps us look at luck as a matter of perspective rather than events.\n" +
            "\n" +
            "2. “The Tortoise and the Hare” by Aesop\n" +
            "Reading Level: Very Easy\n" +
            "\n" +
            "This classic fable tells the story of a very slow tortoise (another word for turtle) and a speedy hare (another word for rabbit). The tortoise challenges the hare to a race. The hare laughs at the idea that a tortoise could run faster than him, but when the two actually race, the results are surprising.\n" +
            "\n" +
            "What Is Great About It: Have you ever heard the English expression, “Slow and steady wins the race”? This story is the basis for that common phrase. This timeless short story teaches a lesson that we all know but can sometimes forget: Natural talent is no substitute for hard work, and overconfidence often leads to failure.\n" +
            "\n" +
            "This short story is available on FluentU, so you can take advantages of all of FluentU’s " +
            "great language-learning features while revisiting this childhood classic.";
}
