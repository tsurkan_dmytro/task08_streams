package epam.task.gener.textreader;

import java.util.List;
import java.util.Map;

public class UniqueWordsController {

    UniqueWordsModel wordsModel;

    public UniqueWordsController() {
        wordsModel = new UniqueWordsModel();
    }

    public long uniqWords()  {
        return wordsModel.uniqWords();

    }

    public List<String> sortUniqWords() {
        return wordsModel.sortUniqWords();

    }

    public Map<String, Integer> collectData() {
        List<String> listWords = getListWords();
        return wordsModel.collectData(listWords);
    }

    public List<String> getListWords() {
        return wordsModel.getListWords();
    }
}
