package epam.task.gener.stremsproj;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreemssModel {

    List<Integer> integerList;

    public StreemssModel() {
         integerList = randIntList();
    }

    public static void main(String[] args) {

    /*taskOneMaxAverage();

    showSum(integerList);
    showMinMax(integerList);
    showAverage(integerList);*/



    }

    public void showAverage(List<Integer> list){
        IntSummaryStatistics stats = list.stream()
                .mapToInt((x) -> x)
                .summaryStatistics();
        System.out.println(stats);
    }

    public void showSum(List<Integer> list){
        Integer sum = list.stream()
                .reduce(0, (a, b) -> a + b);
        System.out.println(sum);
    }

    public void showMinMax(List<Integer> list){
        Integer maxNumber = list.stream()
                .max(Integer::compareTo)
                .get();

        Integer minNumber = list.stream()
                .min(Comparator.comparing(Integer::valueOf))
                .get();

        System.out.println("maxNumber = " + maxNumber);
        System.out.println("minNumber = " + minNumber);
    }

    public List<Integer> randIntList(){
        List<Integer> integers = new Random().ints(10,25,250).boxed().collect(Collectors.toList());
        return integers;
    }

    public static int[] randIntArray(){
        int[] array = new Random().ints(50, 1, 500).toArray();
        return array;
    }

    public void taskOneMaxe(int v, int r, int e){
        Taskone<Integer> maxValue = (x,y,z) -> IntStream.of(x, y, z)
                .max()
                .getAsInt();

        System.out.println("Max value: " + maxValue.calculate(v, r, e));
    }

    public void taskOneAverage(int v, int r, int e){
        Taskone<Integer> averageValue = (x,y,z) -> (x+y+z)/3;

        System.out.println("Average value: " + averageValue.calculate(v, r, e));
    }


    public List<Integer> getIntegerList() {
        return integerList;
    }
}
