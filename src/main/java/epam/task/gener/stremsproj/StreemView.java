package epam.task.gener.stremsproj;

public class StreemView {

    StreemssModel streemssModel;

    public StreemView() {
        streemssModel = new StreemssModel();
    }

    public void taskOneMaxValue(){
        streemssModel.taskOneMaxe(12, 85, 96);
    }

    public void taskOneAvergeValue(){
        streemssModel.taskOneAverage(42, 185, 496);
    }

    public void showAverage(){
        streemssModel.showAverage(streemssModel.getIntegerList());
    }

    public void showSum(){
        streemssModel.showSum(streemssModel.getIntegerList());
    }

    public void showMinMax(){
        streemssModel.showMinMax(streemssModel.getIntegerList());
    }
}
