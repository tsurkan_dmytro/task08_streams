package epam.task.gener;

import epam.task.gener.stremsproj.StreemView;
import epam.task.gener.textreader.UniqueWordsView;

public class MainConsole {

    public static void main(String[] args) {

        StreemView streemView = new StreemView();
        streemView.taskOneMaxValue();
        streemView.taskOneAvergeValue();
        streemView.showSum();
        streemView.showMinMax();
        streemView.showAverage();

        UniqueWordsView wordsView = new UniqueWordsView();
        wordsView.showUniqWords();
        wordsView.showSortedUniqWords();
        wordsView.showCollectData();
    }
}
