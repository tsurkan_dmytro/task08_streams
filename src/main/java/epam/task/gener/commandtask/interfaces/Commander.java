package epam.task.gener.commandtask.interfaces;

public interface Commander
{
    void execute(String s);
}
