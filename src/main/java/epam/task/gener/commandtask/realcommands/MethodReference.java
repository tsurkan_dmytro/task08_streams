package epam.task.gener.commandtask.realcommands;

import epam.task.gener.commandtask.interfaces.Commander;
import epam.task.gener.commandtask.interfaces.FuncInterface;

public class MethodReference implements Commander {
    @Override
    public void execute(String s) {

        FuncInterface functional = MethodReference::saySomething;
        functional.abstractFun(s);

    }

    public static void saySomething(String s){
        System.out.println("Hello, this is static method. Reference" + s);
    }
}
