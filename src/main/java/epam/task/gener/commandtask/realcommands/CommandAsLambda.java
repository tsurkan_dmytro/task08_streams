package epam.task.gener.commandtask.realcommands;

import epam.task.gener.commandtask.interfaces.Commander;
import epam.task.gener.commandtask.interfaces.FuncInterface;

public class CommandAsLambda implements Commander {
    @Override
    public void execute(String s) {

        FuncInterface fobj = (x) -> System.out.println(x);
        fobj.abstractFun(s);
    }
}
