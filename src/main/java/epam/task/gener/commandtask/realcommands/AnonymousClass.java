package epam.task.gener.commandtask.realcommands;

import epam.task.gener.commandtask.interfaces.Commander;
import epam.task.gener.commandtask.interfaces.FuncInterface;

public class AnonymousClass implements Commander {

    @Override
    public void execute(String s) {
        new Ano(s);
    }

    class Ano{
        public Ano(String s) {
            System.out.println(s);
        }
    }
}
